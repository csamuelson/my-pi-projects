import smbus
import time

io_address = 0x20
bus = smbus.SMBus(0)

def quick_read_byte(address):
    try:
        result = bus.read_byte(address)
    except IOError, err:
        print "Error accessing 0x%02X: Check your I2C address" % address
    return result

while True:
    result = quick_read_byte(io_address)
    button = (result & 0x80)
    if button:
        print "up"
        bus.write_byte(io_address,0xf0)
    else:
        print "down"
        bus.write_byte(io_address,0xff)
    time.sleep(0.1)
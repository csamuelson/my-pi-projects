import smbus
import time
import string
from Adafruit_PWM_Servo_Driver import PWM

pwm = PWM(0x40, debug=True)

servoMin = 140  # Min pulse length out of 4096
servoMid = 375
servoMax = 610  # Max pulse length out of 4096

cols = [0xbf,0xdf,0xef]
matrix = {0xbe:'1',0xde:'2',0xee:'3',0xbd:'4',0xdd:'5',0xed:'6',0xbb:'7',0xdb:'8',0xeb:'9',0xb7:'*',0xd7:'0',0xe7:'#'}

io_address = 0x20
bus = smbus.SMBus(0)

pwm.setPWMFreq(60)                        # Set frequency to 60 Hz

def quick_read_byte(address):
    try:
        result = bus.read_byte(address)
    except IOError, err:
        print "Error accessing 0x%02X: Check your I2C address" % address
    return result

def read_matrix():
    matrix_result = ''
    for col in cols:
        bus.write_byte(io_address,col)
        result = quick_read_byte(io_address)
        if matrix.has_key(result):
            #print matrix[result]
            bus.write_byte(io_address,result & 0x7f)
            time.sleep(0.1)
            matrix_result = matrix[result]
    return matrix_result
    
while True:
    result = read_matrix()
    if result != '':
        value = string.atoi(result)
        pwm.setPWM(0, 0, servoMin + (value * 52))
        print result
    time.sleep(0.1)
    


import smbus
import time
eeprom_address = 0x50
io_address = 0x20
bus = smbus.SMBus(0)


def quick_read_byte(address):
    try:
        result = bus.read_byte(address)
    except IOError, err:
        print "Error accessing 0x%02X: Check your I2C address" % address
    return result
    
def set_16bit_location(address,reg):
    (low_byte, high_byte) = (reg & 0xFF, (reg >> 8) & 0xFF)
    try:
        bus.write_byte_data(address, high_byte, low_byte)
    except IOError, err:
        print "Didn't work"
        
 
set_16bit_location(eeprom_address, 0x0000)
result = quick_read_byte(eeprom_address)
print result
 
result = quick_read_byte(eeprom_address)
print result

result = quick_read_byte(eeprom_address)
print result

set_16bit_location(eeprom_address, 0x0001)
result = quick_read_byte(eeprom_address)
print result

result = quick_read_byte(io_address)
print "IO: {0:x}".format(result)

button = (result & 0x04)
print button

while True:
    result = quick_read_byte(io_address)
    button = (result & 0x04)
    if button:
        print "up"
        bus.write_byte(io_address,0x7f)
    else:
        print "down"
        bus.write_byte(io_address,0xff)
    time.sleep(0.1)


import smbus
import time

io_address = 0x20
bus = smbus.SMBus(0)

def quick_read_byte(address):
    try:
        result = bus.read_byte(address)
    except IOError, err:
        print "Error accessing 0x%02X: Check your I2C address" % address
    return result

while True:
    #col0
    bus.write_byte(io_address,0xbf)
    result = quick_read_byte(io_address)
    if result == 0xbe:
        print "1"
    elif result == 0xbd:
        print "4"
    elif result == 0xbb:
        print "7"
    elif result == 0xb7:
        print "*"
    else:
        #col1
        bus.write_byte(io_address,0xdf)
        result = quick_read_byte(io_address)
        if result == 0xde:
            print "2"
            bus.write_byte(io_address,result & 0x7f)
        elif result == 0xdd:
            print "5"
        elif result == 0xdb:
            print "8"
        elif result == 0xd7:
            print "0"
        else:
            #col2
            bus.write_byte(io_address,0xef)
            result = quick_read_byte(io_address)
            if result == 0xee:
                print "3"
            elif result == 0xed:
                print "6"
            elif result == 0xeb:
                print "9"
            elif result == 0xe7:
                print "#"
    time.sleep(0.05)
    
import smbus
import time

cols = [0xbf,0xdf,0xef]
matrix = {0xbe:'1',0xde:'2',0xee:'3',0xbd:'4',0xdd:'5',0xed:'6',0xbb:'7',0xdb:'8',0xeb:'9',0xb7:'*',0xd7:'0',0xe7:'#'}

io_address = 0x20
bus = smbus.SMBus(0)

def quick_read_byte(address):
    try:
        result = bus.read_byte(address)
    except IOError, err:
        print "Error accessing 0x%02X: Check your I2C address" % address
    return result

def read_matrix():
    for col in cols:
        bus.write_byte(io_address,col)
        result = quick_read_byte(io_address)
        if matrix.has_key(result):
            print matrix[result]
            bus.write_byte(io_address,result & 0x7f)
            time.sleep(0.1)

while True:
    read_matrix()
    time.sleep(0.1)
    


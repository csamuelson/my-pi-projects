#!/usr/bin/python

from Adafruit_PWM_Servo_Driver import PWM
import time
import smbus

io_address = 0x20
bus = smbus.SMBus(0)

# ===========================================================================
# Example Code
# ===========================================================================

# Initialise the PWM device using the default address
# bmp = PWM(0x40, debug=True)
pwm = PWM(0x40, debug=True)

servoMin = 140  # Min pulse length out of 4096
servoMid = 375
servoMax = 610  # Max pulse length out of 4096

def setServoPulse(channel, pulse):
  pulseLength = 1000000                   # 1,000,000 us per second
  pulseLength /= 60                       # 60 Hz
  print "%d us per period" % pulseLength
  pulseLength /= 4096                     # 12 bits of resolution
  print "%d us per bit" % pulseLength
  pulse *= 1000
  pulse /= pulseLength
  pwm.setPWM(channel, 0, pulse)
  print "%d pulse" % pulse
  
def quick_read_byte(address):
    try:
        result = bus.read_byte(address)
    except IOError, err:
        print "Error accessing 0x%02X: Check your I2C address" % address
    return result

pwm.setPWMFreq(60)                        # Set frequency to 60 Hz
#while (True):
  # Change speed of continuous servo on channel O
  #pwm.setPWM(0, 0, servoMin)
  #time.sleep(2)
  #pwm.setPWM(0, 0, servoMid)
  #time.sleep(2)
  #pwm.setPWM(0, 0, servoMax)
  #time.sleep(2)

while True:
    result = quick_read_byte(io_address)
    left_button = (result & 0x80)
    right_button = (result & 0x40)
    #if left_button:
        #print "left button up"
        #bus.write_byte(io_address,0xff)
    if not left_button:
        print "left button down"
        bus.write_byte(io_address,0xf0)
        pwm.setPWM(0, 0, servoMin)
    elif right_button:
        #print "right button up"
        bus.write_byte(io_address,0xff)
    else:
        print "right button down"
        bus.write_byte(io_address,0xf0)
        pwm.setPWM(0, 0, servoMax)
    time.sleep(0.1)



